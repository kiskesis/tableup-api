const multer = require('multer'),
      XLSX = require('xlsx');

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    const datetimestamp = Date.now();
    cb(null, file.fieldname + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
  }
});

const upload = multer({
  storage: storage,
  fileFilter : function(req, file, callback) {
    if (['xls', 'xlsx'].indexOf(file.originalname.split('.')[file.originalname.split('.').length-1]) === -1) {
      return callback(new Error('Wrong extension type'));
    }
    callback(null, true);
  }
}).single('file');

class tableAPI {
  constructor () {
  }

  async getTable (res) {
    let endObject = {};
    try {
      let workbook = XLSX.readFile('./uploads/file.xlsx', { sheetStubs: false });

      for (let key in workbook.Sheets) {
        endObject[key] = XLSX.utils.sheet_to_json(workbook.Sheets[key], {header: 1, defval: '\u2028'})
      }
      return res.send(endObject);
    } catch (e) {
      console.log(e);
    }
  }

  async uploadTable(req, res) {
    upload(req, res, function(err){
      if(err){
        res.json({error_code:1, err_desc:err});
        return;
      }

      if(!req.file){
        res.json({error_code:1, err_desc:"No file passed"});
        return;
      }

      try {
        if (err) {
          return res.json({error_code: 1, err_desc: err, data: null});
        }
        res.json({error_code: 0, err_desc: null, data: result});
      } catch (e) {
        res.json({error_code:1, err_desc:"Corupted excel file"});
      }
    });
  }
}

let tableapi = new tableAPI();

module.exports = tableapi;