const tableAPI = require('./tableuploading'),
      express = require('express'),
      router = express.Router();

router.get(('/download'), async (req, res) => {
  try {
    tableAPI.getTable(res);
  } catch (err) {
    res.sendStatus(400);
  }
});

router.post(('/upload'), async (req, res) => {
  try {
    tableAPI.uploadTable(req, res);
  } catch(e) {
    res.sendStatus(400);
  }
});

module.exports = router;