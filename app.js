'use strict';

const express = require('express'),
  bodyParser = require('body-parser'),
  app = express(),
  api = require('./main/api'),
  port = 3001;
// para CORN
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.urlencoded({ extended:false }));
app.use(bodyParser.json());

app.use('/table', api);

const server = app.listen(port, function () {
  console.log("Listening on port %s...", port);
});