# Getting started

## Used versions

- Node: 8.11.3
- NPM: 5.6.0

## Running the app

- `npm install` : install build dependencies

- `npm start` : start server